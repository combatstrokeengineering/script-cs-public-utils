"""Publishes result of Squish test to a MySQL database on the Jenkins Master server (See README section 'Auto-generated Squish Report' for more details)"""

import time
import argparse
from dataclasses import dataclass
from datetime import datetime
from typing import List, Optional, Set, Tuple
import json
import bs4
import numpy as np
import pandas as pd
import pandera as pa
import pytz
import requests
import sqlalchemy
from bs4 import BeautifulSoup
from pandera.typing import Series
from sqlalchemy import MetaData, Table, create_engine
from squish_sheet_utils import create_table_name

# %%
FAILED = "FAILED"
PASSED = "PASSED"
REGRESSED = "REGRESSION"
FLAKY = "FLAKY"
BUILD_BASE_URL = "https://build.cercare-medical.com/job/"
PATH_TO_WEBREPORT_RESULTS = "data/results-v1.js"
MAX_ERROR_DESCRIPTION_LENGTH = 2000
PYTEST_TEST_FOLDERS = ['use-case-tests', 'integration-tests', 'unit-tests', 'non-functional-tests']
UPLOAD_RETRY_COUNT = 3
TIMEOUT_ON_RETRY_SECONDS = 5
@dataclass
class ApiAuth:
    """! Auth API for interacting with the Jenkins master server."""

    user_name: str
    api_token: str

    def auth_tuple(self) -> Tuple:
        """! Returns a tuple with the credentials."""
        return (self.user_name, self.api_token)


@dataclass
class BuildData:
    """! Metadata for a given build.
    
    @param build_url        The url to the build.
    @param build_number     The build number.
    @param job              The job name.
    """
    build_url: str
    build_number: str
    job: str


class TestResultsSchema(pa.SchemaModel):
    job: Series[str] = pa.Field()
    build_number: Series[int] = pa.Field(ge=0)
    suite: Series[str] = pa.Field()
    test: Series[str] = pa.Field()
    duration: Series[str] = pa.Field()
    status: Series[str] = pa.Field()
    error_location: Series[str] = pa.Field()
    error_message: Series[str] = pa.Field()
    error_stacktrace: Series[str] = pa.Field()
    consecutive_fails: Series[int] = pa.Field()
    full_error: Series[str] = pa.Field()
    timestamp: Series[str] = pa.Field()
    build_node: Series[str] = pa.Field()
    build_number_and_job: Series[str] = pa.Field()
    verification_point: Series[str] = pa.Field()


@dataclass
class TestError:
    status: str
    location: str
    message: str
    stacktrace: str
    consecutive_fails: int

    def output(self):
        if self.status == FAILED or self.status == REGRESSED:
            return f"{self.message} --- TRACE: {self.stacktrace} --- LOC: {self.location} \n"
        else:
            return ""


# %%
def get_data_from_http_url(
    url: str, auth: ApiAuth, output_format: str = "xml", verbose: bool = True
) -> requests.models.Response:
    """! Gets data from a given url from the Jenkins web API.
    
    @param url              The url to fetch data from.
    @param auth             The authentication object.
    @param output_format    The format of the output (xml, json or python), defaults to xml.
    @param verbose          Whether to print the status of the HTTP GET request, defaults to True.
    
    @return     The response from the HTTP GET request.
    """
    valid_output_formats = ["xml", "json", "python"]
    assert output_format in valid_output_formats

    api_url = f"{url}api/{output_format}"

    response = requests.get(api_url, auth=auth.auth_tuple(), timeout=60)
    if verbose:
        print("HTTP GET data fetched!") if response.status_code == 200 else print(
            "Build HTTP get error"
        )
    return response


def find_build_with_build_number(
    builds_soup: bs4.BeautifulSoup, build_number: int
) -> Optional[BuildData]:
    """! Looks through the build soup to find build with the specified number.
    
    @param builds_soup      The soup of the builds.
    @param build_number     The build number to look for.

    @return     The build data if found, otherwise None.
    """
    job_name = builds_soup.find("fullName").get_text()
    builds = builds_soup.find_all("build")
    build_number_string = f"{str(build_number)}"
    build_url = search_builds_for_match(
        builds=builds, string=build_number_string, keyword="number"
    )
    if build_url:
        return BuildData(build_url, build_number_string, job_name)
    
    print(f"Didn't find the build number {build_number} in the soup {job_name}")
    return


def search_builds_for_match(
    builds: bs4.BeautifulSoup, string: str, keyword: str
) -> str:
    """! Returns the build url of the first matching build
    
    @param builds       The soup of the builds
    @param string       The string to look for in the keyword
    @param keyword      The keyword relevant to the search
    
    @return     The build url if found, otherwise an empty string
    """
    for build in builds:
        if string == build.find(keyword).get_text():
            build_url = build.url.contents[0]
            return build_url
    return ""


def extract_build_results(build_data: BuildData, auth: ApiAuth) -> pd.DataFrame:
    """! Create pandas dataframe based on the given build data
    
    @param build_data       The build data to extract results from
    @param auth             The authentication object
        
    @return     The pandas dataframe with the results
    """
    test_report_url = f"{build_data.build_url}testReport/"
    test_report_response = get_data_from_http_url(test_report_url, auth, verbose=False)
    test_soup = BeautifulSoup(test_report_response.text, "xml")
    df = extract_results_from_cases(test_soup, build_data)

    build_report_url = f"{build_data.build_url}/"
    build_report_response = get_data_from_http_url(
        build_report_url, auth, verbose=False
    )
    build_soup = BeautifulSoup(build_report_response.text, "xml")
    
    if build_soup.builtOn:
        build_node_name = build_soup.builtOn.get_text()
    else:
        build_node_name = build_soup.displayName.get_text()
    df["build_node"] = build_node_name

    return df


def extract_results_from_cases(
    test_soup: bs4.BeautifulSoup, build_data: BuildData
) -> pd.DataFrame:
    """! Extracts results for each test case in the xml soup
    
    @param test_soup        The soup of the test cases
    @param build_data       The build data to extract results from
    
    @return     The pandas dataframe with the results
    """
    df = pd.DataFrame()
    cases = test_soup.find_all("case")
    for case in cases:
        row = create_case_dataframe(case, build_data)
        if not row.empty:
            df = pd.concat([df, row], axis=0, ignore_index=True)
    return df


def parse_error(case: bs4.ResultSet, status: str) -> TestError:
    """! Construct TestError information for the test case
    
    @param case         The test case to parse
    @param status       The status of the test case

    @return     The TestError object
    """
    if status == FAILED or status == REGRESSED:
        if case.find("name"):
            error_location = case.find("name").get_text()
            error_location = error_location[0:MAX_ERROR_DESCRIPTION_LENGTH]
        else:
            error_location = ""

        if case.errorDetails:
            error_message = case.errorDetails.get_text()
            error_message = error_message[0:MAX_ERROR_DESCRIPTION_LENGTH]
        else:
            error_message = ""

        if case.errorStackTrace:
            error_stacktrace = case.errorStackTrace.get_text()
            error_stacktrace = error_stacktrace[0:MAX_ERROR_DESCRIPTION_LENGTH]
        else:
            error_stacktrace = ""

        consecutive_fails = (
            int(case.age.get_text()) + 1
        )  # The age is 0 when the error appear, so 1 is added to make 'consecutive_fails' make sense
    else:
        status, error_location, error_message, error_stacktrace, consecutive_fails = [
            ""
        ] * len(TestError.__dataclass_fields__.keys())
        consecutive_fails = 0

    return TestError(
        status=status,
        location=error_location,
        message=error_message,
        stacktrace=error_stacktrace,
        consecutive_fails=consecutive_fails,
    )

def create_case_dataframe(case: bs4.ResultSet, build_data: BuildData) -> pd.DataFrame:
    """! Create dataframe with all relevant information from the test case
    
    @param case         The test case to parse
    @param build_data   The build data to extract results from
    
    @return     The pandas dataframe with the results (empty if the test case is cancelled)
    """
    if case.find("name").get_text() != ":-1": #"-1" is the name given for the final test of a cancelled job
        status = case.status.get_text()
        class_name = case.className.get_text()
        test_name = case.find('name').get_text()

        # Logging test case information for easy debugging on failure
        print(f"\nTest case name: {test_name}")
        print(f"Test case class name: {class_name}")
        print(f"Test case status: {status}\n")
        timestamp = pd.Timestamp.now().strftime(
                "%Y-%m-%d %H:%M:%S"
            )

        if not class_name and any(substring in test_name for substring in PYTEST_TEST_FOLDERS): # The class name of a pytest will be missing on pytest collection errors
            test_name = f"tests.{test_name}" 
            case_class_name_elements: List[str] = test_name.split(".")
            suite = ".".join(case_class_name_elements[0:2])
            test = ".".join(case_class_name_elements[2:])
            verification_point = test_name

        elif any(substring in class_name for substring in PYTEST_TEST_FOLDERS):
            class_name = f"tests.{class_name}"
            case_class_name_elements: List[str]  = class_name.split(".")
            suite = ".".join(case_class_name_elements[0:2])
            test = ".".join(case_class_name_elements[2:])
            verification_point = test_name
        elif "use-case-tests" in test_name:
            timestamp = pd.Timestamp(case.stdout.get_text()[0:50].split("|")[0]).strftime(
                "%Y-%m-%d %H:%M:%S"
            )
            suite, test = case.className.get_text().split(".")
            verification_point = test_name.split("use-case-tests")[1][1:]
        else:
            suite, test = case.className.get_text().split(".")
            verification_point = test_name

        error = parse_error(case, status)

        row = pd.DataFrame(
            {
                TestResultsSchema.job: [build_data.job],
                TestResultsSchema.build_number: [build_data.build_number],
                TestResultsSchema.suite: [suite],
                TestResultsSchema.timestamp: [timestamp],
                TestResultsSchema.test: [test],
                TestResultsSchema.duration: [float(case.duration.get_text())],
                TestResultsSchema.status: [status],
                TestResultsSchema.error_location: [error.location],
                TestResultsSchema.error_message: [error.message],
                TestResultsSchema.error_stacktrace: [error.stacktrace],
                TestResultsSchema.consecutive_fails: [error.consecutive_fails],
                TestResultsSchema.full_error: [error.output()],
                TestResultsSchema.verification_point: [verification_point],
                TestResultsSchema.build_number_and_job: [
                    f"{build_data.job}_{build_data.build_number}"
                ],
            }
        )

        return row

    else:
        return pd.DataFrame()


def parse_xml_report(url: str, auth: ApiAuth, build_number: int) -> pd.DataFrame:
    """! Fetches the xml report and parse the information into a dataframe
    
    @param url              The url to fetch data from
    @param auth             The authentication object
    @param build_number     The build number to look for
    
    @return     The pandas dataframe with the results
    """
    response = get_data_from_http_url(url, auth, "xml", verbose=False)
    soup = BeautifulSoup(response.text, "xml")
    build = find_build_with_build_number(soup, build_number)
    if not build:
        return
    df = extract_build_results(build, auth)
    return df


def get_current_date() -> str:
    """! Gets the current date in central Europe
    
    @return     The current date in the format dd/mm/yyyy
    """
    tz_london = pytz.timezone("Europe/London")
    datetime_stamp = datetime.now(tz_london)
    return datetime_stamp.strftime("%d/%m/%Y")


def condense_xml_report(df: pd.DataFrame) -> pd.DataFrame:
    """! Create the condensed test result report
    
    @param df       The dataframe with the full test results
    
    @return     The pandas dataframe with the condensed test results
    """
    df_all = df.groupby(["suite", "test"]).aggregate(
        {
            "duration": sum,
            TestResultsSchema.build_number: pd.Series.mode,
            TestResultsSchema.build_number_and_job: pd.Series.mode,
            "job": pd.Series.mode,
            TestResultsSchema.build_node: pd.Series.mode,
            TestResultsSchema.timestamp: pd.Series.mode,
        }
    )

    df_errors = df[(df["status"] == FAILED) | (df["status"] == REGRESSED)]
    if not df_errors.empty:
        df_errors = df_errors.groupby(["suite", "test"]).aggregate({"full_error": list})
        df_errors[TestResultsSchema.full_error] = df_errors[
            TestResultsSchema.full_error
        ].apply(lambda x: "".join(x))
        df_errors[TestResultsSchema.full_error] = df_errors[
            TestResultsSchema.full_error
        ].apply(lambda x: x[0:4000])
        df_errors["status"] = "FAILED"

        df_combined = pd.merge(
            df_all, df_errors, how="outer", on=["suite", "test"]
        ).reset_index()
        df_combined.loc[df_combined.status.isna(), "status"] = PASSED
        return df_combined
    else:
        df_all[TestResultsSchema.full_error] = np.nan
        df_all[TestResultsSchema.status] = PASSED
        df_all = df_all.reset_index()
        return df_all


def process_xml_report(
    url: str, auth: ApiAuth, build_number: int
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """! Processes the xml report into both full and condensed dataframes
    
    @param url              The url to fetch data from
    @param auth             The authentication object
    @param build_number     The build number to look for
        
    @return     The two pandas dataframes with the full test results and the condensed test results
    """

    print("Fetching the XML report and parsing it")
    df = parse_xml_report(url, auth, build_number)
    df_condensed = condense_xml_report(df)

    df = df.drop("full_error", axis=1)
    current_date = get_current_date()
    df["upload_date"] = current_date
    df_condensed["upload_date"] = current_date
    df_condensed["assigned_developer"] = ""
    return df, df_condensed

def get_jenkins_page(url: str, build_number: str, authentication: ApiAuth, additional_link_arguments: List[str] = []) -> requests.Response:
    """! Fetches the given page of the build as a requests Response
    
    @param url                          The url to fetch data from
    @param build_number                 The build number to look for
    @param authentication               The authentication object
    @param additional_link_arguments    Additional arguments to add to the url (e.g. squish result link)

    @return     The requests Response object
    """
    full_url = f"{url}{build_number}"
    if len(additional_link_arguments) > 0:
        full_url = full_url + "/" + "/".join(additional_link_arguments)

    web_page = requests.get(full_url, auth=authentication.auth_tuple(), timeout=60)
    return web_page

def find_result_link(result: bs4.ResultSet) -> Optional[str]:
    """! Look through a bs4 resultset (from a build page) and find links for squish results
    
    @param result       The bs4 resultset to look through, None if not found

    @return     The squish result link if found, otherwise None
    """
    for link in result.find_all("a", {"class": "task-link"}, href=True):
        if "squishResults" in link['href']:
            return link["href"].split("/")[-1]
    return None

def get_squish_result_links(jenkins_build_page: bs4.BeautifulSoup) -> List[str]:
    """! Find all squish result links for a given bs4 XML tree of a jenkins build page
    
    @param jenkins_build_page       The bs4 XML tree of the jenkins build page

    @return     A list of all the squish result links
    """
    squish_results = []
    for result in jenkins_build_page.find_all("div", {"class": "task"}):
        link = find_result_link(result)
        if link:
            squish_results.append(link)
    
    return squish_results

def find_web_report_link(url: str, build_number: str, authentication: ApiAuth, squish_result_link: str) -> Optional[str]:
    """! Find the build web report link for a given squish result link
    
    @param url                  The url to fetch data from
    @param build_number         The build number to look for
    @param authentication       The authentication object
    @param squish_result_link   The squish result link to look for
        
    @return     The web report link if found, otherwise None
    """
    squish_result = get_jenkins_page(url, build_number, authentication, [squish_result_link])
    squish_result_soup = BeautifulSoup(squish_result.text, 'lxml')
    
    for result in squish_result_soup.find_all("a", href=True):
        if "webreport" in result["href"]:
            webreport_name = result["href"].split("/")[0]

    if webreport_name:
        return webreport_name

def get_web_report_links(base_url: str, build_number: str, authentication: ApiAuth, jenkins_squish_result_links: List[str]) -> Set[str]:
    """! Get all the squish result web report links for a given build
    
    @param base_url                     The base url of the jenkins server
    @param build_number                 The build number to look for
    @param authentication               The authentication object
    @param jenkins_squish_result_links  The list of squish result links to look through
    
    @return     A set of all the web report links
    """
    web_report_links = set()
    for squish_result_link in jenkins_squish_result_links:
        webreport_name = find_web_report_link(base_url, build_number, authentication, squish_result_link)
        if webreport_name:
            web_report_links.add(f"{base_url}{build_number}/{squish_result_link}/{webreport_name}/{PATH_TO_WEBREPORT_RESULTS}")

    return web_report_links

def get_suite_jsons(suite_texts: List[str]) -> List[dict]:
    """! From a given text for a suite result, get the json dictionaries
    
    @param suite_texts      The list of suite texts to look through
    
    @return     A list of all the json dictionaries representing the suites
    """
    jsons = []
    for suite_text in suite_texts[1:]: # The first entry is a script command, not a json dict
        json_text = suite_text[suite_text.find('{') : suite_text.rfind('}')+1]
        suite_json = json.loads(json_text)
        jsons.append(suite_json)
    return jsons

def get_testsuite_jsons(web_report_links: Set[str], authentication: ApiAuth) -> List[dict]:
    """! Extract the jsons from the jenkins build web report links
    
    @param web_report_links     The set of web report links to look through
    @param authentication       The authentication object
    
    @return     A list of all the json dictionaries representing the testsuites
    """
    testsuite_jsons = []
    for web_report_link in web_report_links:
        web_report_text = requests.get(web_report_link, auth=authentication.auth_tuple(), timeout=60).text
        
        suite_texts = web_report_text.split("data.push( ")
        
        webreport_jsons = get_suite_jsons(suite_texts)
        testsuite_jsons.extend(webreport_jsons)

    return testsuite_jsons

def identify_flaky_test(test_dict: dict) -> Optional[str]:
    """! Identifies if the test given is flaky
    
    @param test_dict        The test dictionary to look through
    
    @return     The name of the test if it is flaky, otherwise None
    """
    test_name = test_dict.get("name")
    retry_count = test_dict.get("retry").get("count")
    if retry_count < 1:
        return None
    fail = False

    for verification_point_dict in test_dict.get("tests"):
        verification_point_result = verification_point_dict.get("result")
        if verification_point_result in ["FAIL", "ERROR", REGRESSED, "FATAL"]:
            fail = True

    if fail is False: # If the test is retried but passed all tests, it must have succeeded but failed beforehand
        return test_name

    return None

def find_flaky_tests_in_suite(suite_json: dict) -> List[dict]:
    """! Finds the flaky test in the given testsuite json
    
    @param suite_json       The testsuite json to look through
    
    @return     A list of dictionaries with the suite name and test name of the flaky tests
    """
    suite_name = suite_json.get("tests")[0].get("name")
    flaky_test_names = []
    for test_dict in suite_json.get("tests")[0].get("tests"):
        flaky_test_name = identify_flaky_test(test_dict)
        if flaky_test_name:
            flaky_test_names.append(flaky_test_name)

    if len(flaky_test_names) == 0:
        return {}

    flaky_tests_dicts = [{"suite_name": suite_name, "test_name": test_name} for test_name in flaky_test_names]
    return flaky_tests_dicts

def update_dataframe_with_flaky_tests(testsuite_jsons: List[dict], df: pd.DataFrame) -> None:
    """! Marks the flaky tests in the dataframe based on the list of testsuite jsons
    
    @param testsuite_jsons      The list of testsuite jsons with flaky tests
    @param df                   The dataframe to update
    """
    flaky_test_dicts = []
    for suite_json in testsuite_jsons:
        flaky_tests = find_flaky_tests_in_suite(suite_json)
        if flaky_tests:
            flaky_test_dicts.extend(flaky_tests)

    print(f"Found {len(flaky_test_dicts)} flaky tests, updating the condensed dataframe")
                
    for flaky_test_dict in flaky_test_dicts:
        suite_name = flaky_test_dict.get("suite_name")
        test_name = flaky_test_dict.get("test_name")
        df.loc[(df["suite"] == suite_name) & (df["test"] == test_name), ['status']] = "FLAKY"


def identify_and_mark_flaky_test(url: str, build_number: str, authentication: ApiAuth, df: pd.DataFrame) -> None:
    """! Identifies and marks the flaky tests in the dataframe
    
    @param url              The url to fetch data from
    @param build_number     The build number to look for
    @param authentication   The authentication object
    @param df               The dataframe to update
    """

    jenkins_build_page = get_jenkins_page(url, build_number, authentication)
    web_page_soup = BeautifulSoup(jenkins_build_page.text, 'lxml')

    jenkins_squish_result_links = get_squish_result_links(web_page_soup)
    web_report_links = get_web_report_links(url, build_number, authentication, jenkins_squish_result_links)
    testsuite_jsons = get_testsuite_jsons(web_report_links, authentication)

    update_dataframe_with_flaky_tests(testsuite_jsons, df)

# %%
def create_condensed_mysql_table(
    connection_engine: sqlalchemy.engine.Engine, metadata: MetaData, condensed_table_name: str
) -> None:
    """! Creates the condensed MySQL table if it doesn't exist
    
    @param connection_engine    The connection engine to the MySQL database
    @param metadata             The metadata of the build to add to the database
    """
    exist = sqlalchemy.inspect(connection_engine).has_table(condensed_table_name)
    if not exist:
        print(f"=Table {condensed_table_name} doesn't exist=")
        condensed_table = Table(
            condensed_table_name,
            metadata,
            sqlalchemy.Column("timestamp", sqlalchemy.TIMESTAMP(), primary_key=False),
            sqlalchemy.Column(
                "build_number_and_job",
                sqlalchemy.String(200),
                nullable=False,
                primary_key=True,
            ),
            sqlalchemy.Column("upload_date", sqlalchemy.String(50), primary_key=False),
            sqlalchemy.Column("job", sqlalchemy.String(200), primary_key=False),
            sqlalchemy.Column("build_number", sqlalchemy.String(200), nullable=False),
            sqlalchemy.Column(
                "suite", sqlalchemy.String(200), nullable=False, primary_key=True
            ),
            sqlalchemy.Column("test", sqlalchemy.String(200), primary_key=True),
            sqlalchemy.Column(
                "duration", sqlalchemy.Float(precision=2), nullable=False
            ),
            sqlalchemy.Column("status", sqlalchemy.String(200), nullable=False),
            sqlalchemy.Column("build_node", sqlalchemy.String(200), nullable=False),
            sqlalchemy.Column(
                "assigned_developer", sqlalchemy.String(200), nullable=True
            ),
            sqlalchemy.Column("full_error", sqlalchemy.String(5000), nullable=True),
        )
        metadata.create_all(connection_engine)
        print("=Table was created=")
    else:
        print(f"Table {condensed_table_name} already exists")


def main():
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        "--jenkins_username", required=True, help="Jenkins server username"
    )
    argument_parser.add_argument(
        "--jenkins_password", required=True, help="Jenkins server password"
    )
    argument_parser.add_argument(
        "--mysql_username", required=True, help="MySQL database username"
    )
    argument_parser.add_argument(
        "--mysql_password", required=True, help="MySQL database password"
    )
    argument_parser.add_argument(
        "--mysql_server", required=True, help="MySQL database password"
    )
    argument_parser.add_argument(
        "--mysql_port", required=True, help="MySQL database password"
    )
    argument_parser.add_argument(
        "--build_job",
        required=True,
        help="The specific job name to look for (master-win-app-ui or master-linux-app-ui, for example))",
    )
    argument_parser.add_argument(
        "--build_number",
        required=True,
        help="The specific build number to look for (e.g '1697'",
    )

    args = argument_parser.parse_args()

    url = f"{BUILD_BASE_URL}{args.build_job}/"
    condensed_table_name = create_table_name(build_job=args.build_job, build_number=int(args.build_number))
    # %%
    jenkins_authentication = ApiAuth(args.jenkins_username, args.jenkins_password)

    # %%
    print(f"Processing the XML report for build {args.build_number} from {url}")
    df, df_condensed = process_xml_report(
        url, jenkins_authentication, args.build_number
    )

    print("Identifying flaky tests from Jenkins webpages and marking them in the dataframe")
    try:
        identify_and_mark_flaky_test(url, args.build_number, jenkins_authentication, df_condensed)
    except Exception as error:
        print(f"Failed to identify and mark flaky tests due to the following error: {error}")

    print("Setting up the MySQL connection")
    mysql_connection_engine = create_engine(
        f"mysql+pymysql://{args.mysql_username}:{args.mysql_password}@{args.mysql_server}:{args.mysql_port}/squish_report"
    )

    metadata = MetaData(mysql_connection_engine)

    create_condensed_mysql_table(connection_engine=mysql_connection_engine, metadata=metadata, condensed_table_name=condensed_table_name)

    index = True  # Optional, set to True if you want to include the index
    csv_file_path = "df_condensed.csv"  # Change this to your desired file path
    print(f"Saving the condensed dataframe at {csv_file_path} as .csv file for debugging")
    df_condensed.to_csv(csv_file_path, index=index)
    
    print("Uploading the data to the MySQL database tables")

    print(f"Uploading {len(df_condensed)} entries to the MySQL database raw table")
    
    for i in range(UPLOAD_RETRY_COUNT):
        try:
            df_condensed.to_sql(
                condensed_table_name,
                if_exists="replace",
                con=mysql_connection_engine,
                index=False,
                method=None,
            )
            break
        except Exception as error:
            print(f"Failed to upload the data to the MySQL database due to the following error: {error}")
            print(f"Retrying to upload the data to the MySQL database, attempt {i+1}")
            time.sleep(TIMEOUT_ON_RETRY_SECONDS)

    print("Success")
    return 0

main()
