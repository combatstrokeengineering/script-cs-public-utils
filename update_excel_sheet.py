"""
Extracts Squish test results from the MySQL database on the Jenkins Master server and updates the Sharepoint Squish Excel sheet with the information
(See README section 'Auto-generated Squish Report' for more details)
"""

import argparse
import os
from dataclasses import dataclass
from datetime import datetime, timedelta
from pathlib import Path
from typing import List, Optional, Tuple
import re
import openpyxl
import pandas as pd
import numpy as np
from office365.runtime.auth.authentication_context import AuthenticationContext
from office365.sharepoint.client_context import ClientContext
from openpyxl.workbook.protection import FileSharing
from openpyxl import load_workbook
from openpyxl.styles import Alignment, Border, Font, PatternFill, Side
from openpyxl.utils import get_column_letter
from sqlalchemy import create_engine
from squish_sheet_utils import create_table_name

# %%

AUTO_EXCEL_SHEET_DIR_PATH = "/sites/Developers534/Delte dokumenter/Squish"
AUTO_EXCEL_SHEET_NAME = "Squish_Report.xlsx"
AUTO_EXCEL_SHEET_RELATIVE_PATH = AUTO_EXCEL_SHEET_DIR_PATH + "/" + AUTO_EXCEL_SHEET_NAME
AUTO_EXCEL_SHEET_LOCAL_PATH = "auto_excel_sheet.xlsx"
COMMENT_COLUMN_WIDTH = 60
BUILD_STATUS_COLUMN_WIDTH = 10


@dataclass
class BuildMetaInformation:
    """! Meta information about the build found in the mysql-extracted dataframe

    @param initial_timestamp_date   The date of the build
    @param platform                 The platform of the build
    @param build_node               The node of the build
    @param build_number             The number of the build
    """
    initial_timestamp_date: str
    platform: str
    build_node: str
    build_number: int


DATE_ROW = 1
PLATFORM_ROW = 2
NODE_ROW = 3
BUILD_NUMBER_ROW = 4
RELEVANT_BUILD_AGE = 8
FIRST_COLUMN_INDEX = 1

TOTAL_ROW_SHIFT = 2
PRESUMED_FIXED_ROW_SHIFT = 4
FIXED_ROW_SHIFT = 5
FAILED_ROW_SHIFT = 6
FLAKY_ROW_SHIFT = 7
VERY_FLAKY_ROW_SHIFT = 8
CANNOT_REPRODUCE_ROW_SHIFT = 9

red_fill = PatternFill(start_color="FFFF0000", end_color="FFFF0000", fill_type="solid")

purple_fill = PatternFill(
    start_color="ff7030a0", end_color="ff7030a0", fill_type="solid"
)

orange_fill = PatternFill(
    start_color="FFED7D31", end_color="FFED7D31", fill_type="solid"
)

green_fill = PatternFill(
    start_color="ff00b050", end_color="ff00b050", fill_type="solid"
)

yellow_fill = PatternFill(
    start_color="FFFFFF00", end_color="FFFFFF00", fill_type="solid"
)

gray_fill = PatternFill(
    start_color="FF9E9E9E", end_color="FF9E9E9E", fill_type="solid"
)

no_fill = PatternFill(start_color="ffffffff", end_color="ffffffff", fill_type="solid")

FLAKY_COLORS = [yellow_fill, orange_fill]
INVALID_BUILD_NUMBER = 0
# %%
def create_sharepoint_context(
    sharepoint_client_id: str, sharepoint_client_secret: str, site_url: str
) -> ClientContext:
    """! Creates the context used to communicate with the specific Sharepoint site parsed as an argument

    @param sharepoint_client_id         The client id of the Sharepoint site
    @param sharepoint_client_secret     The client secret of the Sharepoint site
    @param site_url                     The url of the Sharepoint site
    
    @return         The ClientContext used to communicate with the Sharepoint site
    """
    context_auth = AuthenticationContext(site_url)
    context_auth.acquire_token_for_app(
        client_id=sharepoint_client_id, client_secret=sharepoint_client_secret
    )

    ctx = ClientContext(site_url, context_auth)
    web = ctx.web
    ctx.load(web)
    ctx.execute_query()

    if not ctx:
        print("E: Failed to create sharepoint context.")
        return None

    print(f"Web title: {web.properties['Title']}")

    return ctx


# %%
def download_sharepoint_file(
    ctx: ClientContext, server_relative_url: str, output_path: str
) -> Path:
    """! Downloads the relative url given from the ctx's (context's) Sharepoint site
    
    @param ctx                      The context used to communicate with the Sharepoint site
    @param server_relative_url      The relative url of the file to download
    @param output_path              The path to download the file to
    
    @return     The path of the downloaded file
    """
    file_path = os.path.abspath(output_path)
    with open(file_path, "wb") as local_file:
        file = ctx.web.get_file_by_server_relative_url(server_relative_url)
        file.download(local_file)
        ctx.execute_query()

    print(f"Your file is downloaded here: {file_path}")


# %%
def add_base_rows(worksheet: openpyxl.worksheet.worksheet.Worksheet) -> None:
    """! Adds the leftmost basic row titles of the worksheet
    
    @param worksheet        The worksheet to add the rows to
    """

    date_cell = worksheet.cell(row=DATE_ROW, column=FIRST_COLUMN_INDEX)
    date_cell.value = "Date"

    platform_cell = worksheet.cell(row=PLATFORM_ROW, column=FIRST_COLUMN_INDEX)
    platform_cell.value = "Platform"

    node_cell = worksheet.cell(row=NODE_ROW, column=FIRST_COLUMN_INDEX)
    node_cell.value = "Node"

    build_cell = worksheet.cell(row=BUILD_NUMBER_ROW, column=FIRST_COLUMN_INDEX)
    build_cell.value = "Build"

    for cell in [date_cell, platform_cell, node_cell, build_cell]:
        cell.font = Font(bold=True)
        cell.alignment = Alignment(horizontal="left")


def is_name_in_column(
    name: str,
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
    column_index: Optional[int] = FIRST_COLUMN_INDEX
) -> bool:
    """! Checks if a given name is found in the column
    
    @param name             The name to check for
    @param worksheet        The worksheet to check in
    @param column_index     The column index to check in. Defaults to FIRST_COLUMN_INDEX.
        
    @return bool        True if the name is found in the column, False otherwise
    """
    for column in worksheet.iter_cols(min_col=column_index, max_col=column_index):
        for cell in column:
            if cell.value == name:
                return True
    return False


def find_first_empty(
    column_index: int, worksheet: openpyxl.worksheet.worksheet.Worksheet
) -> Optional[openpyxl.cell.cell.Cell]:
    """! Finds the first empty cell in the given column
    
    @param column_index     The column index to check in
    @param worksheet        The worksheet to check in
        
    @return         The first empty cell in the column, None if no empty cell is found
    """
    for column in worksheet.iter_rows(min_col=column_index, max_col=column_index):
        for cell in column:
            if cell.value is None:
                return cell
            if cell.row == worksheet.max_row:
                worksheet.append([None])
                return worksheet.cell(row=worksheet.max_row, column=column_index)
    return None


def add_non_existing_suites(
    df: pd.DataFrame, worksheet: openpyxl.worksheet.worksheet.Worksheet
) -> None:
    """! Adds all the missing suites (found in the dataframe) to the worksheet
    
    @param df               The dataframe to extract the suites from
    @param worksheet        The worksheet to add the suites to
    """
    for suite in df["suite"].unique():
        found = is_name_in_column(suite, worksheet)
        if not found:
            total_summary_cell = find_cell(worksheet, FIRST_COLUMN_INDEX, "Total")
            if total_summary_cell:
                worksheet.insert_rows(total_summary_cell.row, 1)
            empty_cell = find_first_empty(FIRST_COLUMN_INDEX, worksheet)
            empty_cell.value = suite
            empty_cell.font = Font(bold=True)


def add_non_existing_tests(
    df: pd.DataFrame, worksheet: openpyxl.worksheet.worksheet.Worksheet
) -> None:
    """! Adds all the missing tests of the suite categories to the worksheet
    
    @param df               The dataframe to extract the tests from
    @param worksheet        The worksheet to add the tests to
    """
    for suite in df["suite"].unique():
        non_relevant_suites = df["suite"].unique()
        non_relevant_suites = non_relevant_suites[non_relevant_suites != suite]
        non_relevant_suites = np.append(non_relevant_suites, "Total")
        for test in df[df["suite"] == suite]["test"].unique():
            test_cell = test_cell_identifier(
                suite, test, worksheet, non_relevant_suites
            )
            test_cell.value = test
            test_cell.alignment = Alignment(horizontal="left")
            test_cell.font = Font(name='Consolas', size=11)


def find_last_non_empty_cell(
    worksheet: openpyxl.worksheet.worksheet.Worksheet, column: int
) -> Optional[openpyxl.cell.cell.Cell]:
    """! Finds the last non-empty cell of a given column
    
    @param worksheet    The worksheet to check in
    @param column       The column index to check in

    @return     The last non-empty cell in the column, None if no non-empty cell is found
    """
    last_cell = None
    for cell in worksheet[get_column_letter(column)][5:]:
        if cell.value is not None:
            last_cell = cell

    return last_cell


def change_summary_cell(
    last_cell: openpyxl.cell.cell.Cell,
    shift: int,
    color: PatternFill,
    text: str,
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
) -> None:
    """! Changes the lastmost summary cells, using a shift from the last non-empty cell to find the correct position
    
    @param last_cell        The last non-empty cell of the column
    @param shift            The shift from the last non-empty cell to find the correct position
    @param color            The color to fill the cell with
    @param text             The text to insert into the cell
    @param worksheet        The worksheet to add the summary cell to
    """
    cell = worksheet.cell(last_cell.row + shift, FIRST_COLUMN_INDEX)
    cell.value = text
    cell.fill = color


def add_summary_rows(worksheet: openpyxl.worksheet.worksheet.Worksheet) -> None:
    """! Adds the lastmost summary/informational rows to the first column of the worksheet
    
    @param worksheet        The worksheet to add the summary rows to
    """
    total_found = is_name_in_column("Total", worksheet)
    if not total_found:
        last_cell = find_last_non_empty_cell(worksheet, FIRST_COLUMN_INDEX)

        total_cell = worksheet.cell(last_cell.row + TOTAL_ROW_SHIFT, FIRST_COLUMN_INDEX)
        total_cell.value = "Total"
        total_cell.font = Font(bold=True)

        change_summary_cell(
            last_cell,
            PRESUMED_FIXED_ROW_SHIFT,
            purple_fill,
            "Presumed Fixed",
            worksheet,
        )
        change_summary_cell(last_cell, FIXED_ROW_SHIFT, green_fill, "Fixed", worksheet)
        change_summary_cell(last_cell, FAILED_ROW_SHIFT, red_fill, "Failed", worksheet)
        change_summary_cell(last_cell, FLAKY_ROW_SHIFT, yellow_fill, "Flaky", worksheet)
        change_summary_cell(
            last_cell,
            VERY_FLAKY_ROW_SHIFT,
            orange_fill,
            "Very Flaky",
            worksheet,
        )
        change_summary_cell(last_cell, CANNOT_REPRODUCE_ROW_SHIFT, gray_fill, "Cannot Reproduce", worksheet)


def test_cell_identifier(
    suite: str,
    test: str,
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
    other_suites: List[str],
    column_index="A",
) -> Optional[openpyxl.cell.cell.Cell]:
    """! Finds or creates the test cell of the given suite in the worksheet
    
    @param suite            The suite of the test
    @param test             The test to find or create
    @param worksheet        The worksheet to find or create the test cell in
    @param other_suites     The other suites to check for the test in
    @param column_index     The column index to check in. Defaults to "A".
    
    @return     The test cell of the given suite in the worksheet, None if no test cell is found or created
    """
    found_suite = False
    for cell in worksheet[column_index]:
        if found_suite is False and cell.value == suite:
            found_suite = True

        if found_suite is True and (cell.value is None or cell.value in other_suites):
            worksheet.insert_rows(cell.row, 1)
            return worksheet.cell(row=cell.row - 1, column=cell.column)

        elif found_suite is True and cell.value == test:
            return cell

        elif found_suite is True and cell.row == worksheet.max_row:
            worksheet.append([None])
            return worksheet.cell(worksheet.max_row, cell.column)
    return None


def remove_empty_rows(worksheet: openpyxl.worksheet.worksheet.Worksheet) -> None:
    """! Removes the empty rows from the worksheet
    
    @param worksheet        The worksheet to remove the empty rows from
    """
    for row in worksheet.rows:
        deletion = False
        if all(cell.value == None for cell in row):
            deletion = True

        if deletion is True:
            worksheet.delete_rows(row[0].row, 1)
            remove_empty_rows(worksheet)
            return


def set_horizontal_border(
    worksheet: openpyxl.worksheet.worksheet.Worksheet, row: int, size: str = "medium"
) -> None:
    """! Sets the size the bottom border of the row
    
    @param worksheet    The worksheet to set the border in
    @param row          The row to set the border in
    @param size         The size of the border. Defaults to "medium".
    """
    border_style = Side(border_style=size, color="000000")
    for cell in worksheet[row]:
        cell.border = Border(top=border_style)


def extract_meta_information(df: pd.DataFrame) -> BuildMetaInformation:
    """! Extracts the build's meta-information from the dataframe
    
    @param df   The dataframe to extract the meta-information from
    
    @return     The build's meta-information
    """
    print("This is the timestamp value and type")
    print(df["timestamp"].sort_values(ascending=True).iloc[0])
    print(type(df["timestamp"].sort_values(ascending=True).iloc[0]))
    initial_timestamp: str = df["timestamp"].sort_values(ascending=True).iloc[0]
    initial_timestamp_date = initial_timestamp.split(" ")[0].replace("/", "-")
    platform = df.iloc[0]["job"]
    build_node = df.iloc[0]["build_node"]
    build_number = df.iloc[0]["build_number"]
    return BuildMetaInformation(
        initial_timestamp_date, platform, build_node, build_number
    )


def find_build_information_column(
    meta_info: BuildMetaInformation, worksheet: openpyxl.worksheet.worksheet.Worksheet
) -> Optional[str]:
    """! Finds the column corresponding to the build identified in the metadata
    
    @param meta_info    The metadata of the build to find
    @param worksheet    The worksheet to find the build in

    @return     The column corresponding to the build identified in the metadata, None if no build is found
    """
    for cell in worksheet[DATE_ROW]:
        if (
            cell.value == meta_info.initial_timestamp_date
            and worksheet.cell(PLATFORM_ROW, cell.column).value == meta_info.platform
        ):
            return cell.column
    return None


def identify_column_insertion_location(
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
    meta_info: BuildMetaInformation,
) -> str:
    """! Finds or creates the columns that will be used to insert further information into
    
    @param worksheet    The worksheet to find or create the columns in
    @param meta_info    The metadata of the build to find or create the columns for

    @return     The column index (integer value as string) of the first column of the build
    """
    for cell in worksheet[DATE_ROW]:
        if cell.column > 1 and cell.value is not None:
            cell_date = None
            print(f"This is the date of the column: {cell.value}")
            if type(cell.value) is str:
                cell_date = datetime.strptime(cell.value, "%Y-%m-%d")
            else:
                cell_date = cell.value

        if (
            cell.column > 1
            and cell.value is not None
            and datetime.strptime(meta_info.initial_timestamp_date, "%Y-%m-%d")
            >= cell_date
        ):
            column_value_before_insert = cell.column
            worksheet.insert_cols(cell.column, 2)
            return column_value_before_insert
        elif cell.column == worksheet.max_column:
            return cell.column + 1
        elif (
            cell.column > 1
            and type(cell).__name__ != "MergedCell"
            and cell.value is None
        ):
            return cell.column


def insert_meta_information(
    column: str,
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
    meta_info: BuildMetaInformation,
) -> None:
    """! Inserts and formats the metadata for a given build into the worksheet
    
    @param column       The column index (integer value as string) of the first column of the build
    @param worksheet    The worksheet to insert the metadata into
    @param meta_info    The metadata of the build to insert
    """
    worksheet.column_dimensions[
        get_column_letter(int(column))
    ].width = BUILD_STATUS_COLUMN_WIDTH
    worksheet.column_dimensions[
        get_column_letter(int(column) + 1)
    ].width = COMMENT_COLUMN_WIDTH

    worksheet.cell(DATE_ROW, column).value = meta_info.initial_timestamp_date
    worksheet.cell(DATE_ROW, column).alignment = Alignment(horizontal="center")

    worksheet.cell(PLATFORM_ROW, column).value = meta_info.platform
    worksheet.cell(PLATFORM_ROW, column).alignment = Alignment(horizontal="center")

    worksheet.cell(NODE_ROW, column).value = meta_info.build_node
    worksheet.cell(NODE_ROW, column).alignment = Alignment(horizontal="center")

    build_link = f"https://build.cercare-medical.com/job/{meta_info.platform}/{meta_info.build_number}/"
    worksheet.cell(BUILD_NUMBER_ROW, column).value = '=HYPERLINK("{}", "{}")'.format(
        build_link, meta_info.build_number
    )
    worksheet.cell(BUILD_NUMBER_ROW, column).style = "Hyperlink"
    worksheet.cell(BUILD_NUMBER_ROW, column).alignment = Alignment(horizontal="center")

    worksheet.cell(BUILD_NUMBER_ROW, column + 1).value = "Comments"
    worksheet.cell(BUILD_NUMBER_ROW, column + 1).alignment = Alignment(
        horizontal="center"
    )

    for row in [DATE_ROW, PLATFORM_ROW, NODE_ROW]:
        worksheet.merge_cells(
            start_row=row, start_column=column, end_row=row, end_column=column + 1
        )


def find_cell(
    worksheet: openpyxl.worksheet.worksheet.Worksheet, column_index: int, name: str
) -> Optional[openpyxl.cell.cell.Cell]:
    """! Finds the cell with the given name in the column
    
    @param worksheet        The worksheet to find the cell in
    @param column_index     The column index to find the cell in
    @param name             The name of the cell to find
    
    @return     The cell with the given name in the column, None if no cell is found
    """
    for cell in worksheet[get_column_letter(column_index)]:
        if cell.value == name:
            return cell
    return None

def update_test_cell_with_error_information(data_row: pd.Series, column_location: str, worksheet: openpyxl.worksheet.worksheet.Worksheet, color: PatternFill, additional_comment_prefix: str = "") -> None:
    """! Updates a specific test cell with error information from failed or flaky tests
    
    @param data_row                     The row of the dataframe containing the error information
    @param column_location              The column index (integer value as string) of the first column of the build
    @param worksheet                    The worksheet to update the test cell in
    @param color                        The color to fill the cell with
    @param additional_comment_prefix    The prefix to add to the error message. Defaults to "".
    """
    test_cell = test_cell_identifier(data_row.suite, data_row.test, worksheet, [])
    test_result_cell = worksheet.cell(row=test_cell.row, column=column_location)
    test_result_cell.fill = color
    test_result_cell.value = " "

    test_comment_cell = worksheet.cell(
        row=test_cell.row, column=int(column_location) + 1
    )
    error_message = data_row.full_error
    if error_message:
        edited_error_message = re.sub('(Screenshot in.*?png")', '', error_message)
        edited_error_message = edited_error_message.replace("() ", "")
        test_comment_cell.value = f"{additional_comment_prefix}{edited_error_message}"
    else:
        test_comment_cell.value = f"{additional_comment_prefix}. No error message recorded"
    test_comment_cell.alignment = Alignment(horizontal="fill")


def update_test_cell_with_old_error_information(data_row: pd.Series, column_location: str, previous_column_location: str, worksheet: openpyxl.worksheet.worksheet.Worksheet) -> None:
    """! Updates a specific test cell with error information from previous failed tests
    
    @param data_row                     The row of the dataframe containing the error information
    @param column_location              The column index (integer value as string) of the first column of the build
    @param previous_column_location     The column index (integer value as string) of the first column of the previous build
    @param worksheet                    The worksheet to update the test cell in
    """
    test_cell = test_cell_identifier(data_row.suite, data_row.test, worksheet, [])
    old_test_cell_value = worksheet.cell(row=test_cell.row, column=previous_column_location).value
    
    if old_test_cell_value:
        test_result_cell = worksheet.cell(row=test_cell.row, column=column_location)
        test_result_cell.value = old_test_cell_value

def insert_errors(
    column_location: str,
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
    df: pd.DataFrame,
) -> None:
    """! Applies the errors of the dataframe by colorings spaces and inserting error messages
    
    @param column_location          The column index (integer value as string) of the first column of the build
    @param worksheet                The worksheet to apply the errors to
    @param df                       The dataframe containing the errors to apply
    """
    df_errors = df[df["status"] == "FAILED"]

    if df_errors.empty:
        return

    for _, error_row in df_errors.iterrows():
        update_test_cell_with_error_information(error_row, column_location, worksheet, red_fill)

    total_summary_cell = find_cell(worksheet, FIRST_COLUMN_INDEX, "Total")
    if total_summary_cell:
        worksheet.cell(total_summary_cell.row, column_location).value = len(df_errors)


def insert_flaky_tests(
    column_location: str,
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
    df: pd.DataFrame,
) -> None:
    """! Applies the flaky tests of the dataframe by colorings spaces and inserting messages
    
    @param column_location      The column index (integer value as string) of the first column of the build
    @param worksheet            The worksheet to apply the flaky tests to
    @param df                   The dataframe containing the flaky tests to apply
    """
    df_flaky = df[df["status"] == "FLAKY"]

    if df_flaky.empty:
        return

    for _, flaky_row in df_flaky.iterrows():
        update_test_cell_with_error_information(flaky_row, column_location, worksheet, yellow_fill, "Flaky! ")

def mark_very_flaky_tests(column_location: str, meta_info: BuildMetaInformation, number_of_previous_flaky_days: int, worksheet: openpyxl.worksheet.worksheet.Worksheet, df: pd.DataFrame) -> None:
    """! Marks tests as very flaky if they have been flaky for the last number_of_previous_flaky_days
    
    @param column_location                      The column index (integer value as string) of the first column of the build
    @param meta_info                            The metadata of the build to find or create the columns for
    @param number_of_previous_flaky_days        The number of previous flaky days to check for
    @param worksheet                            The worksheet to apply the flaky tests to
    @param df                                   The dataframe containing the flaky tests to apply
    """
    df_flaky = df[df["status"] == "FLAKY"]
    if df_flaky.empty:
        return
    
    previous_build_columns = []
    for build_age_in_days in range(1, number_of_previous_flaky_days + 1):
        previous_build_column = find_previous_build_information_column(build_age_in_days, meta_info, worksheet)
        if previous_build_column:
            previous_build_columns.append(previous_build_column)

    if not len(previous_build_columns) == number_of_previous_flaky_days:
        return

    for _, flaky_row in df_flaky.iterrows():
        test_cell = test_cell_identifier(flaky_row.suite, flaky_row.test, worksheet, [])
        test_result_cell = worksheet.cell(row=test_cell.row, column=column_location)
        very_flaky = True
        for previous_build_column in previous_build_columns:
            previous_build_result_cell = worksheet.cell(row=test_cell.row, column=previous_build_column)
            if previous_build_result_cell.fill not in FLAKY_COLORS:
                very_flaky = False

        if very_flaky:
            test_result_cell.fill = orange_fill

def mark_flaky_crash_as_error(
    column_location: str,
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
    df: pd.DataFrame,
) -> None:
    """! Applies the flaky tests of the dataframe by colorings spaces and inserting messages
    
    @param column_location      The column index (integer value as string) of the first column of the build
    @param worksheet            The worksheet to apply the flaky tests to
    @param df                   The dataframe containing the flaky tests to apply
    """
    df_flaky = df[df["status"] == "FLAKY"]

    if df_flaky.empty:
        return

    for _, flaky_row in df_flaky.iterrows():
        if flaky_row.full_error:
            if "Lost connection to AUT" in flaky_row.full_error or "crashed" in flaky_row.full_error:
                update_test_cell_with_error_information(flaky_row, column_location, worksheet, red_fill, "Flaky Crash! ")

def insert_old_error_information(
    column_location: str,
    meta_info: BuildMetaInformation,
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
    df: pd.DataFrame,
) -> None:
    """! Applies the error comments from a previous build to the current errors
    
    @param column_location      The column index (integer value as string) of the first column of the build
    @param meta_info            The metadata of the build to find or create the columns for
    @param worksheet            The worksheet to apply the error comments to
    @param df                   The dataframe containing the error comments to apply
    """
    one_day_old_previous_build_column_location = find_previous_build_information_column(1, meta_info, worksheet)

    if not one_day_old_previous_build_column_location:
        return
    
    df_errors = df[df["status"] == "FAILED"]

    if df_errors.empty:
        return

    for _, error_row in df_errors.iterrows():
        update_test_cell_with_old_error_information(error_row, column_location, one_day_old_previous_build_column_location, worksheet)

def add_meta_information(
    df: pd.DataFrame, worksheet: openpyxl.worksheet.worksheet.Worksheet
) -> bool:
    """! Adds the build metadata to the worksheet
    
    @param df           The dataframe to extract the metadata from
    @param worksheet    The worksheet to add the metadata to
    
    @return bool    True if the metadata was added, False if the build already exists in the worksheet
    """
    meta_info = extract_meta_information(df)

    found_build = find_build_information_column(meta_info, worksheet)
    if found_build:
        print("Build already added to sheet, skipping adding meta/error information")
        return False

    column_location = identify_column_insertion_location(worksheet, meta_info)

    insert_meta_information(column_location, worksheet, meta_info)
    return True

def find_previous_build_information_column(previous_build_age_days: int, meta_information: BuildMetaInformation, worksheet: openpyxl.worksheet.worksheet.Worksheet) -> Optional[str]:
    """! Finds the column corresponding to an older build corresponding to the current date of the metadata offset with the given number of days
    
    @param previous_build_age_days      The number of days to offset the current date of the metadata
    @param meta_information             The metadata of the current build
    @param worksheet                    The worksheet to find the build in
    
    @return         The column corresponding to the older build, None if no build is found
    """
    current_date = datetime.strptime(meta_information.initial_timestamp_date, '%Y-%m-%d')
    previous_build_timestamp = (current_date - timedelta(days=previous_build_age_days)).strftime("%Y-%m-%d")
    previous_build_meta_information = BuildMetaInformation(
        previous_build_timestamp, meta_information.platform, meta_information.build_node, INVALID_BUILD_NUMBER
    )
    return find_build_information_column(previous_build_meta_information, worksheet)

def add_error_information(
    df: pd.DataFrame, worksheet: openpyxl.worksheet.worksheet.Worksheet
) -> None:
    """! Adds the error information to the worksheet
    
    @param df           The dataframe to extract the error information from
    @param worksheet    The worksheet to add the error information to
    """
    meta_info = extract_meta_information(df)
    column_location = find_build_information_column(meta_info, worksheet)

    if column_location:
        insert_errors(column_location, worksheet, df)
        insert_flaky_tests(column_location, worksheet, df)
        insert_old_error_information(column_location, meta_info, worksheet, df)
        mark_very_flaky_tests(column_location, meta_info, 2, worksheet, df)
        mark_flaky_crash_as_error(column_location, worksheet, df)

    return


def hide_old_information(worksheet: openpyxl.worksheet.worksheet.Worksheet) -> None:
    """! Hides test rows without recent errors and all old build columns
    
    @param worksheet        The worksheet to hide the information in
    """
    for row in worksheet.rows:
        row_index = row[0].row
        worksheet.row_dimensions[row_index].hidden = False
        if row[0].value is not None and any(substring in row[0].value for substring in ['test_', 'tst_']):
            failed_lately = False
            for cell in row[1 : RELEVANT_BUILD_AGE * 2 + 1]:
                if cell.value:
                    failed_lately = True
            if not failed_lately:
                worksheet.row_dimensions[row_index].hidden = True
            else:
                worksheet.row_dimensions[row_index].hidden = False    

    for column_index in range(1, worksheet.max_column + 1):
        worksheet.column_dimensions[get_column_letter(column_index)].hidden = False

    for column_index in range(RELEVANT_BUILD_AGE * 2 + 2, worksheet.max_column + 1):
        worksheet.column_dimensions[get_column_letter(column_index)].hidden = True


def reorder_builds(
    column: Tuple, worksheet: openpyxl.worksheet.worksheet.Worksheet
) -> None:
    """! Reorders the columns by inserting new columns, copying to them and deleting old columns
    
    @param column       The column to reorder
    @param worksheet    The worksheet to reorder the column in
    """
    column_index = column[0].column
    worksheet.insert_cols(column[0].column - 2, 2)
    column_index += 2
    for cell in column:
        new_cell = worksheet.cell(cell.row, column_index - 4)
        new_cell.value = cell.value
        if cell.fill.fill_type is not None:
            color = cell.fill.fgColor.rgb
            new_cell.fill = PatternFill(
                start_color=color, end_color=color, fill_type="solid"
            )
            worksheet.cell(new_cell.row, new_cell.column + 1).alignment = Alignment(
                horizontal="fill"
            )
        if cell.row == BUILD_NUMBER_ROW:
            new_cell.style = "Hyperlink"

    for cell in worksheet[get_column_letter(column_index + 1)]:
        new_cell = worksheet.cell(cell.row, column_index - 3)
        new_cell.value = cell.value

    worksheet.delete_cols(column_index, 2)


def check_if_builds_are_disordered(
    column: Tuple,
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
    first_build_preference: str,
) -> bool:
    """! Checks if the dates of two builds match and if the first_build_preference build is the first of the two builds listed
    
    @param column                   The column to check
    @param worksheet                The worksheet to check the column in
    @param first_build_preference   The first build preference to check for

    @return         True if the builds are disordered, False if not
    """
    if (
        column[0].value is not None
        and column[0].value
        == worksheet.cell(DATE_ROW, column[DATE_ROW].column - 2).value
    ):
        if first_build_preference in column[1].value:
            return True
    return False


def order_os_builds(
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
    first_build_preference: str = "linux",
) -> None:
    """! Reorders the builds for the same date so that they appear in consistent order
    
    @param worksheet                The worksheet to reorder the builds in
    @param first_build_preference   The first build order preference (win or linux). Defaults to "linux".
    """
    for column in worksheet.columns:
        if column[0].column > 3:
            disorder = check_if_builds_are_disordered(
                column, worksheet, first_build_preference
            )
            if disorder:
                reorder_builds(column, worksheet)


def restore_formatting_of_builds(
    worksheet: openpyxl.worksheet.worksheet.Worksheet,
) -> None:
    """! Reformats build metadata rows as a countermeasure to the effect of inserting rows (which breaks formatting)
    
    @param worksheet    The worksheet to reformat the build metadata rows in
    """
    for column in worksheet.columns:
        column_index = column[0].column

        if column_index == worksheet.max_column:
            return

        if worksheet.cell(BUILD_NUMBER_ROW, int(column_index) + 1).value == "Comments":

            worksheet.column_dimensions[
                get_column_letter(int(column_index))
            ].width = BUILD_STATUS_COLUMN_WIDTH
            worksheet.column_dimensions[
                get_column_letter(int(column_index) + 1)
            ].width = COMMENT_COLUMN_WIDTH

            worksheet.cell(DATE_ROW, column_index).alignment = Alignment(
                horizontal="center"
            )
            worksheet.cell(PLATFORM_ROW, column_index).alignment = Alignment(
                horizontal="center"
            )

            worksheet.cell(NODE_ROW, column_index).alignment = Alignment(
                horizontal="center"
            )

            worksheet.cell(BUILD_NUMBER_ROW, column_index).alignment = Alignment(
                horizontal="center"
            )

            worksheet.cell(BUILD_NUMBER_ROW, column_index + 1).alignment = Alignment(
                horizontal="center"
            )

            for row in [DATE_ROW, PLATFORM_ROW, NODE_ROW]:
                worksheet.merge_cells(
                    start_row=row,
                    start_column=column_index,
                    end_row=row,
                    end_column=column_index + 1,
                )


def process_dataframe_and_sheet(
    df: pd.DataFrame, worksheet: openpyxl.worksheet.worksheet.Worksheet
) -> None:
    """! Updates the worksheet using the data found in the dataframe
    
    @param df           The dataframe to extract the data from
    @param worksheet    The worksheet to update
    """
    add_base_rows(worksheet)
    add_non_existing_suites(df, worksheet)
    add_non_existing_tests(df, worksheet)
    add_summary_rows(worksheet)
    if add_meta_information(df, worksheet):
        add_error_information(df, worksheet)
    order_os_builds(worksheet)
    hide_old_information(worksheet)
    restore_formatting_of_builds(worksheet)
    set_horizontal_border(worksheet, 4)
    set_horizontal_border(worksheet, find_cell(worksheet, 1, "Total").row)

# %%
def main():
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("--site_url", required=True, help="Sharepoint site")
    argument_parser.add_argument(
        "--sharepoint_client_id", required=True, help="Sharepoint client ID"
    )
    argument_parser.add_argument(
        "--sharepoint_client_secret", required=True, help="Sharepoint client secret"
    )
    argument_parser.add_argument(
        "--mysql_username", required=True, help="MySQL database username"
    )
    argument_parser.add_argument(
        "--mysql_password", required=True, help="MySQL database password"
    )
    argument_parser.add_argument(
        "--mysql_server", required=True, help="MySQL server IP"
    )
    argument_parser.add_argument(
        "--mysql_port", required=True, help="MySQL server port"
    )
    argument_parser.add_argument(
        "--build_job",
        required=True,
        help="The specific job name to look for (master-win-app-ui or master-linux-app-ui, for example))",
    )
    argument_parser.add_argument(
        "--build_number",
        required=True,
        help="The specific build number to look for (e.g '1697')",
    )
    argument_parser.add_argument(
        "--target_sheet",
        required=True,
        help="The sheet in the Squish Excel file to add information to."
    )

    args = argument_parser.parse_args()

    condensed_table_name = create_table_name(build_job=args.build_job, build_number=int(args.build_number))
    print(f"Creating sharepoint context at website {args.site_url}")
    ctx = create_sharepoint_context(
        args.sharepoint_client_id, args.sharepoint_client_secret, args.site_url
    )
    if not ctx:
        return 1

    print(f"Downloading the excel worksheet at {AUTO_EXCEL_SHEET_RELATIVE_PATH}")
    download_sharepoint_file(
        ctx, AUTO_EXCEL_SHEET_RELATIVE_PATH, AUTO_EXCEL_SHEET_LOCAL_PATH
    )

    print("Creating mysql engine in order to fetch build data")
    cnx = create_engine(
        f"mysql+pymysql://{args.mysql_username}:{args.mysql_password}@{args.mysql_server}:{args.mysql_port}/squish_report"
    )

    print(
        f"Fetching build test data rom the MySQL database table {condensed_table_name}"
    )
    df = pd.read_sql(
        f"SELECT * FROM {condensed_table_name}",
        cnx,
    )

    print(f"Loading the excel workbook at {AUTO_EXCEL_SHEET_LOCAL_PATH}")
    workbook = load_workbook(filename=AUTO_EXCEL_SHEET_LOCAL_PATH)
    workbook.fileSharing = FileSharing(readOnlyRecommended=True)

    worksheet_name = args.target_sheet

    if worksheet_name not in workbook.sheetnames:
        _ = workbook.create_sheet(worksheet_name)
    worksheet: openpyxl.worksheet.worksheet.Worksheet = workbook[worksheet_name]
    
    print("Updating the excel workbook")
    process_dataframe_and_sheet(df, worksheet)

    print(
        f"Saving the result of dataframe and excel workbook processing to {AUTO_EXCEL_SHEET_LOCAL_PATH}"
    )
    workbook.save(filename=AUTO_EXCEL_SHEET_LOCAL_PATH)

    print(f"Uploading the results to {AUTO_EXCEL_SHEET_DIR_PATH}")
    target_folder = ctx.web.get_folder_by_server_relative_url(
        AUTO_EXCEL_SHEET_DIR_PATH
    )
    with open(AUTO_EXCEL_SHEET_LOCAL_PATH, "rb") as content_file:
        file_content = content_file.read()
        target_folder.upload_file(AUTO_EXCEL_SHEET_NAME, file_content).execute_query()

    print("Success!")
    return 0


# %%
main()
