set -x
set +e

function application_submodules_reset {
    if [ -e test-data ]
    then
        cd test-data/

        git reset --hard
        git lfs fetch
        cd $WORKING_DIR
    fi

    if [ -e binary ]
    then
        cd binary

        git reset --hard
        git lfs fetch
        cd $WORKING_DIR
    fi

    if [ -e cercare-python ]
    then
        cd cercare-python

        git reset --hard
        git lfs fetch
        cd $WORKING_DIR
    fi

    if [ -e license-handler ]
    then
        cd license-handler

        git reset --hard
        git lfs fetch
        cd $WORKING_DIR
    fi

    if [ -e cercare-services ]
    then
        cd cercare-services

        git reset --hard
        git lfs fetch
        cd $WORKING_DIR
    fi
}

function initialize_documentation_submodule {
    git submodule init documentation
    git submodule set-branch --branch ${sourceBranch} documentation
    git submodule update --remote documentation
}

echo "Performing post-SCM-pull cleaning and fetching of submodules..."

while getopts b: flag
do
    case "${flag}" in
        b) argument_build_type=${OPTARG};;
    esac
done

BUILD_TYPE=${argument_build_type:='application'}

echo "Build type is set to: $BUILD_TYPE";

WORKING_DIR=`pwd`

if [ $BUILD_TYPE == "documentation" ]
then
    initialize_documentation_submodule
fi

if [ -e documentation ]
then
    cd documentation
    git reset --hard
    git lfs fetch
    cd $WORKING_DIR
fi

if [ $BUILD_TYPE == "application" ]
then
    application_submodules_reset
fi

echo "Post-SCM-pull cleaning and fetching of submodules done..."

