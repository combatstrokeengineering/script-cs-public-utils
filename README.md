# README #

Public utilities and scripts in use at Cercare Medical

### Auto-generated Squish Report
The publish_test_restuls_to_mysql.py and update_excel_sheet.py are used in Jenkins jobs to generate the report found at https://combatstroke.sharepoint.com/:x:/r/sites/Developers534/_layouts/15/Doc.aspx?sourcedoc=%7B82DE60FE-1AA5-4FAD-8694-B9A56AC0DD38%7D&file=Squish_Report.xlsx&action=default&mobileredirect=true&cid=967e1fc1-d231-4c25-8ea0-6f8ee53c6a72

The update_mysql_database job on Jenkins (https://build.cercare-medical.com/view/Squish%20Report%20Status%20/job/update_mysql_database/) uses publish_test_restuls_to_mysql.py to upload the test report results to the mysql database running on the Jenkins master server.
The update_excel_sheet job on Jenkins (https://build.cercare-medical.com/view/Squish%20Report%20Status%20/job/update_excel_sheet/) uses update_excel_sheet.py to fetch data from the mysql database and update the excel sheet on sharepoint. The script is idempotent, but will not overwrite old results if multiple UI jobs have been run on the same day.

Both jobs run automatically after a UI job has completed, but can also be run manually from Jenkins. This requires the user to input the OS type (Linux/Windows) and the specific build number of the UI build information they want to add to the sheet.
