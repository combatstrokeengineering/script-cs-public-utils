def create_table_name(build_job: str, build_number: int):
    """Create a MySQL compatible table name from input args"""
    return f"{build_job.replace('-', '_')}_{build_number}"
