#!/bin/bash

# Echo commands before executing them.
# Should improve debuggability.
set -x

# Disable exiting on error
set +e

while getopts b: flag
do
    case "${flag}" in
        b) argument_build_type=${OPTARG};;
    esac
done

BUILD_TYPE=${argument_build_type:='application'}

echo "Build type is set to: $BUILD_TYPE";

echo "Performing pre-build cleaning..."

WORKING_DIR=`pwd`

# We leak storescp processes, which locks dll's in the repo
# so we need to kill those processes before attempting to clean
function is_cygwin {
        if [ -e /usr/bin/cygcheck.exe ]
        then
                return 1
        fi
        return 0
}

function kill_storescp {
        is_cygwin
        IS_CYGWIN=$?

        if [ $IS_CYGWIN == 1 ]
        then
                echo "Killing rogue storescp.exe processes"
                taskkill /F /IM storescp.exe
        fi
}

function kill_filebeat {
        is_cygwin
        IS_CYGWIN=$?

        if [ $IS_CYGWIN == 1 ]
        then
                echo "Killing rogue filebeat.exe processes"
                taskkill /F /IM filebeat.exe
        fi
}

function submodule_cleanup {
    git fetch --prune
    git add -u
    git reset --hard
}

kill_storescp
kill_filebeat
git config --global credential.helper store

# Remove main repository lock file
rm -f .git/index.lock

# If we are in an app context
rm -f .git/modules/lib-cercaremedical/modules/binary-cercaremedical/index.lock
rm -f lib-cercaremedical/binary-cercaremedical/.gitattributes
rm -f lib-cercaremedical/binary-cercaremedical/.gitignore

rm -f .git/modules/lib-cercaremedical/modules/test-data/index.lock
rm -f lib-cercaremedical/test-data/.gitattributes
rm -f lib-cercaremedical/test-data/.gitignore

rm -f .git/modules/lib-cercaremedical/modules/lib-cs-buildutils/index.lock
rm -f .git/modules/lib-cercaremedical/test-data/index.lock
rm -f .git/modules/lib-cercaremedical/binary-cercaremedical/index.lock
rm -rf lib-cercaremedical/lib-cs-buildutils/.gitattributes
rm -rf lib-cercaremedical/lib-cs-buildutils/.gitignore


# Submodules in mono repo app context
rm -f .git/modules/test-data/index.lock
rm -rf test-data/.gitattributes
rm -rf test-data/.gitignore

rm -f .git/modules/binary/index.lock
rm -rf binary/.gitattributes
rm -rf binary/.gitignore

rm -f .git/modules/documentation/index.lock
rm -rf documentation/.gitattributes
rm -rf documentation/.gitignore

rm -f .git/modules/cercare-python/index.lock
rm -rf cercare-python/.gitattributes
rm -rf cercare-python/.gitignore

rm -f .git/modules/license-handler/index.lock
rm -rf license-handler/.gitattributes
rm -rf license-handler/.gitignore

rm -f .git/modules/cercare-services/index.lock
rm -rf cercare-services/.gitattributes
rm -rf cercare-services/.gitignore

# If we are in a lib context
rm -f .git/modules/binary-cercaremedical/index.lock
rm -f binary-cercaremedical/.gitattributes
rm -f binary-cercaremedical/.gitignore

rm -f .git/modules/test-data/index.lock
rm -f test-data/.gitattributes
rm -f test-data/.gitignore

rm -f .git/modules/lib-cs-buildutils/index.lock
rm -rf lib-cs-buildutils/.gitattributes
rm -rf lib-cs-buildutils/.gitignore

# Remove build artefacts folders
rm -rf bin/*
rm -rf build/*
rm -rf installers/*

if [ $BUILD_TYPE == "application" ]
then
    if [ -e test-data ]
    then
        cd test-data/
        submodule_cleanup

        cd $WORKING_DIR
    fi

    if [ -e binary ]
    then
        cd binary
        submodule_cleanup

        cd $WORKING_DIR
    fi

    if [ -e cercare-python ]
    then
        cd cercare-python
        submodule_cleanup

        cd $WORKING_DIR
    fi

    if [ -e license-handler ]
    then
        cd license-handler
        submodule_cleanup

        cd $WORKING_DIR
    fi

    if [ -e cercare-services ]
    then
        cd cercare-services
        submodule_cleanup

        cd $WORKING_DIR
    fi
fi

if [ -e documentation ]
then
    cd documentation/
    submodule_cleanup
    git clean -fdx
    cd $WORKING_DIR
fi


if [ -e lib-cercaremedical/binary-cercaremedical ]
then
	cd lib-cercaremedical/binary-cercaremedical/
	submodule_cleanup

	cd $WORKING_DIR
fi
if [ -e lib-cercaremedical/test-data ]
then
	cd lib-cercaremedical/test-data/
	submodule_cleanup

	cd $WORKING_DIR
fi
if [ -e lib-cercaremedical/lib-cs-buildutils ]
then
	cd lib-cercaremedical/lib-cs-buildutils/
	submodule_cleanup

	cd $WORKING_DIR
fi

if [ -e binary-cercaremedical ]
then
	cd binary-cercaremedical/
	submodule_cleanup

	cd $WORKING_DIR
fi
if [ -e lib-cs-buildutils ]
then
	cd lib-cs-buildutils/
	submodule_cleanup

	cd $WORKING_DIR
fi

# Ensure that git is configured with contact information
# so that Jenkins can make merge commits
git config --global user.email "developers@cercare-medical.com"
git config --global user.name "Jenkins"


echo "Manual execution of 'Clean Before Checkout' Jenkins step (without 10 minute timeout)"
echo "Cleaning workspace"
git rev-parse --verify HEAD
echo "Resetting working tree"
git reset --hard
git clean -ffdx

if [ $BUILD_TYPE == "application" ]
then
    git submodule foreach --recursive git reset --hard
    git submodule foreach --recursive git clean -fdx
fi


if [ $BUILD_TYPE == "application" ]
then
    # Make sure that any updates to the submodule URLs are
    # reflected locally. This handles migrating from https to ssh.
    git submodule sync --recursive

    # If someone nuked the submodule dirs, attempt to recreate them
    # in a simple but consistent state
    git submodule update --init --recursive
fi

echo "Pre-build cleaning complete"
